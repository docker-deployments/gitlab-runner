#!/usr/bin/env bash
if ! grep -q "git.insomnia247.nl" "/etc/gitlab-runner/config.toml"; then
  echo "Custom runner registration"
  gitlab-runner register \
    --non-interactive \
    --url "https://git.insomnia247.nl" \
    --token "${RUNNER_TOKEN}" \
    --executor "docker" \
    --docker-image alpine:latest \
    --description "${HOSTNAME}-docker-runner" \
    --docker-services-limit 8
  echo "Runner registration complete"
fi

echo "Starting gitlab-runner..."
/entrypoint run --user=gitlab-runner --working-directory=/home/gitlab-runner